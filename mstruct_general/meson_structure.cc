#include <JANA/JFactoryGenerator.h>

#include "MesonStructureProcessor.h"

extern "C"
{
    void InitPlugin(JApplication *app)
    {
        InitJANAPlugin(app);

        app->Add(new MesonStructureProcessor(app));
        app->Add(new JFactoryGeneratorT<JFactoryT<MesonStructureInputParticle>>());
    }
}
