#include <JANA/JFactoryGenerator.h>

#include "MesonStructureLambdaProcessor.h"

extern "C"
{
    void InitPlugin(JApplication *app)
    {
        InitJANAPlugin(app);

        app->Add(new MesonStructureLambdaProcessor(app));
        app->Add(new JFactoryGeneratorT<JFactoryT<MesonStructureInputParticle>>());
    }
}
