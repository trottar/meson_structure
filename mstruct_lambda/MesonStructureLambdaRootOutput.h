#ifndef MESON_STRUCTURE_LAMBDA_ROOT_OUTPUT_HEADER
#define MESON_STRUCTURE_LAMBDA_ROOT_OUTPUT_HEADER

#include <TFile.h>
#include <TDirectory.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TApplication.h>

class MesonStructureLambdaRootOutput
{
public:
    void init(TFile *file)
    {
        // 'locker' locks mutex so other threads can't interfere with TFile doing its job
        std::lock_guard<std::recursive_mutex> locker(lock);

        // create a subdirectory "hist_dir" in this file
        plugin_root_dir = file->mkdir("mstruct_lambda");
        file->cd();         // Just in case some other root file is the main TDirectory now

        // TTree with recoiled electron
        tree_rec_e = new TTree("rec_e", "a Tree with vect");
        tree_rec_e->SetDirectory(plugin_root_dir);

        //------------------------------------ High-x  -----------------------------------------------------
        const int XBINS_h=19, Q2BINS_h=16;
        double xEdges_h[XBINS_h + 1] =
                {-3.8, -3.6, -3.4, -3.2, -3.,
                 -2.8, -2.6, -2.4, -2.2, -2.,
                 -1.8, -1.6, -1.4, -1.2, -1.,
                 -0.8, -0.6, -0.4, -0.2, 0.};


        Double_t Q2Edges_h[Q2BINS_h + 1] =
                {0.,   0.25,  0.5,  0.75, 1.,
                 1.25, 1.5,   1.75, 2.,   2.25,
                 2.5,  2.75,  3.,   3.25, 3.5,
                 3.75, 4.};


 //       h2_XQ2_true = new TH2I("h2_XQ2_true","True X,Q2 ", XBINS_h,xEdges_h, Q2BINS_h, Q2Edges_h);
        h2_XQ2_true = new TH2I("h2_XQ2_true","True X,Q2 ", 100,0.,1., 100, 1.,300.);
        h2_XQ2_true->SetDirectory(plugin_root_dir);
        h2_XQ2_true->GetXaxis()->SetTitle("x_{BJ}");
        h2_XQ2_true->GetYaxis()->SetTitle("Q^{2}");
 //.......................................
        h2_XQ2_true_cuts = new TH2I("h2_XQ2_true_cut","True X,Q2 cuts ", 100,0.,1., 100, 1.,300.);
        h2_XQ2_true_cuts->SetDirectory(plugin_root_dir);
        h2_XQ2_true_cuts->GetXaxis()->SetTitle("x_{BJ}");
        h2_XQ2_true_cuts->GetYaxis()->SetTitle("Q^{2}");

        h1_Q2_true = new TH1D("h1_Q2_true","True Q2 ",100, 0,300);
        h1_Q2_true->SetDirectory(plugin_root_dir);
        h1_Q2_true->GetXaxis()->SetTitle("Q2_{BJ}");

        h1_X_true = new TH1D("h1_X_true","True X ",100, 0.,1.);
        h1_X_true->SetDirectory(plugin_root_dir);
        h1_X_true->GetXaxis()->SetTitle("X_{BJ}");



        //----- electron Method --------------------
  //      h2_XQ2_em = new TH2I("h2_XQ2_em","Em X,Q2 ", XBINS_h,xEdges_h, Q2BINS_h, Q2Edges_h);
        h2_XQ2_em = new TH2I("h2_XQ2_em","Em X,Q2 ", 100,0.,1., 100, 1.,300.);
        h2_XQ2_em->SetDirectory(plugin_root_dir);
        h2_XQ2_em->GetXaxis()->SetTitle("x_{BJ}");
        h2_XQ2_em->GetYaxis()->SetTitle("Q^{2}");

        h2_XQ2_em_cuts = new TH2I("h2_XQ2_em_cuts ","El X,Q2 cuts ", XBINS_h,xEdges_h, Q2BINS_h, Q2Edges_h);
        h2_XQ2_em_cuts->SetDirectory(plugin_root_dir);
        h2_XQ2_em_cuts->GetXaxis()->SetTitle("log(x)");
        h2_XQ2_em_cuts->GetYaxis()->SetTitle("log(Q^{2})");

        h1_Q2_em = new TH1D("h1_Q2_em","Q2 em",100, 0,300);
        h1_Q2_em->SetDirectory(plugin_root_dir);
        h1_Q2_em->GetXaxis()->SetTitle("Q2^{EM}_{BJ}");

        h1_X_em = new TH1D("h1_X_em","X  em",100, 0.,1.);
        h1_X_em->SetDirectory(plugin_root_dir);
        h1_X_em->GetXaxis()->SetTitle("X^{EM}_{BJ}");


        h2_XQ2_4vec = new TH2I("h2_XQ2_4vec","X,Q2  4vec", 100,0.,1., 100, 1.,300.);
        h2_XQ2_4vec->SetDirectory(plugin_root_dir);
        h2_XQ2_4vec->GetXaxis()->SetTitle("x_{BJ}");
        h2_XQ2_4vec->GetYaxis()->SetTitle("Q^{2}");

        h1_Q2_4vec = new TH1D("h1_Q2_4vec","Q2 4vec",100, 0,300);
        h1_Q2_4vec->SetDirectory(plugin_root_dir);
        h1_Q2_4vec->GetXaxis()->SetTitle("Q2^{4vec}_{BJ}");

        h1_X_4vec = new TH1D("h1_X_4vec","X 4vec",100, 0.,1.);
        h1_X_4vec->SetDirectory(plugin_root_dir);
        h1_X_4vec->GetXaxis()->SetTitle("X^{4vec}_{BJ}");
//=============================================================================================
  //-------------------Sub particles (electron) -------------------------------

        h_El_Etot= new TH1D("h_El_Etot", " energy of electron", 500,0.,30.);
        h_El_Etot->GetXaxis()->SetTitle("E [GeV]");
        h_El_Etot->SetDirectory(plugin_root_dir);

        h_El_Theta = new TH1D("h_El_Theta", " Theta of electron", 100,0.,180.);
        h_El_Theta->GetXaxis()->SetTitle("Theta [deg]");
        h_El_Theta->SetDirectory(plugin_root_dir);
//=====================================================================
//-------------------Sub particles (pion) -------------------------------

        h_Pion_Etot= new TH1D("h_Pion_Etot", " energy of Pion", 500,0.,100.);
        h_Pion_Etot->GetXaxis()->SetTitle("E [GeV]");
        h_Pion_Etot->SetDirectory(plugin_root_dir);

        h_Pion_Theta = new TH1D("h_Pion_Theta", " Theta of Pion", 100,0.,180.);
        h_Pion_Theta->GetXaxis()->SetTitle("Theta [deg]");
        h_Pion_Theta->SetDirectory(plugin_root_dir);

        h_PionPtot = new TH1D("h_PionPtot", " Total momentum of pion", 100,0.,100.);
        h_PionPtot->GetXaxis()->SetTitle("p [GeV]");
        h_PionPtot->SetDirectory(plugin_root_dir);
//=====================================================================
        //-------------------Sub particles ( proton) -------------------------------
        h_ProtonPtot = new TH1D("h_ProtonPtot", " Total momentum of proton", 500,0.,500.);
        h_ProtonPtot->GetXaxis()->SetTitle("p [GeV]");
        h_ProtonPtot->SetDirectory(plugin_root_dir);

        h_ProtonPt = new TH1D("h_ProtonPt", " PT of proton", 500,0.,500.);
        h_ProtonPt->GetXaxis()->SetTitle("pt [GeV]");
        h_ProtonPt->SetDirectory(plugin_root_dir);

        h_ProtonTheta = new TH1D("h_ProtonTheta", " Theta of proton", 100,0.,1.);
        h_ProtonTheta->GetXaxis()->SetTitle("Theta [deg]");
        h_ProtonTheta->SetDirectory(plugin_root_dir);


        //-------------------t, theta-------------------------------
        h_t_pt_p = new TH1D("h_t_pt_p", "  t ( proton) ", 100,0.,1.);
        h_t_pt_p->GetXaxis()->SetTitle("t [GeV^2]");
        h_t_pt_p->SetDirectory(plugin_root_dir);

        h_t_pt_p_Theta = new TH2I("h_t_pt_p_Theta", "  t ( proton) vs Theta ",100,0.,1., 100,0.,10.);
        h_t_pt_p_Theta->GetYaxis()->SetTitle("t [GeV^2]");
        h_t_pt_p_Theta->GetXaxis()->SetTitle("Theta [deg]");
        h_t_pt_p_Theta->SetDirectory(plugin_root_dir);
//------------------ Hits in Roman Pots -------------------------------
        h2_RP_XY = new TH2I("h2_RP_XY", " Hits in Roman Pots XY ",1000,-150.,50., 1000,-100.,100.);
        h2_RP_XY->GetXaxis()->SetTitle("x [cm]");
        h2_RP_XY->GetYaxis()->SetTitle("y[cm]");
        h2_RP_XY->SetDirectory(plugin_root_dir);

        h_t_pt_p_RP = new TH1D("h_t_pt_p_RP", "  t ( proton)  RP ", 100,0.,1.);
        h_t_pt_p_RP->GetXaxis()->SetTitle("t [GeV^2]");
        h_t_pt_p_RP->SetDirectory(plugin_root_dir);


        h_t_pt_p_Theta_RP = new TH2I("h_t_pt_p_Theta_RP", "  t ( proton) vs Theta with hits in RP",100,0.,1., 100,0.,10.);
        h_t_pt_p_Theta_RP->GetYaxis()->SetTitle("t [GeV^2]");
        h_t_pt_p_Theta_RP->GetXaxis()->SetTitle("Theta [deg]");
        h_t_pt_p_Theta_RP->SetDirectory(plugin_root_dir);
        //-----------------------------------------------------------
        hEventN = new TH1D("EventN", "Event N", 100,0.,1000.);
        hEventN->GetXaxis()->SetTitle("Event Number");

        hEventN->SetDirectory(plugin_root_dir);
    }

    // ---- kinematic variables---

    TH2I *h2_XQ2_true, *h2_XQ2_true_cuts;
    TH1D *h1_X_true, *h1_Q2_true;

    TH2I *h2_XQ2_em, *h2_XQ2_4vec;
    TH1D *h1_X_em, *h1_Q2_em,*h1_X_4vec, *h1_Q2_4vec;
    TH2I  *h2_XQ2_em_cuts;
 //================================================
 //............ Electron................
 TH1D *h_El_Theta,*h_El_Etot;
//............ Pion ................
 TH1D *h_Pion_Theta,*h_Pion_Etot,*h_PionPtot;

 //.............Proton...................
     //..................................................
     TH1D *h_t_pt_p;
     TH2I  *h_t_pt_p_Theta;
    //..................................................
     TH2I *h_t_pt_p_Theta_RP, *h2_RP_XY;
     TH1D *h_t_pt_p_RP;
    TH1D *h_ProtonPtot,*h_ProtonPt,*h_ProtonTheta;

     TH1D *hEventN;
    std::recursive_mutex lock;

    TTree * tree_rec_e;     // Tree to store electron related data

private:

    TDirectory* plugin_root_dir;   // Main TDirectory for Plugin histograms and data

};

#endif // OPEN_CHARM_ROOT_OUTPUT_HEADER