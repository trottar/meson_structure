#include "TRandom.h"
#include "TLorentzVector.h"
#include "TVector3.h"

#include <fmt/core.h>

#include <JANA/JEvent.h>
#include <dis/functions.h>
#include <MinimalistModel/McFluxHit.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <ejana/EStringHelpers.h>

#include "MesonStructureLambdaProcessor.h"

#include <TClingRuntime.h>
#include <fmt/format.h>     // For format and print functions
#include "Math/Vector4D.h"
#include <ejana/EServicePool.h>
#include <ejana/plugins/io/lund_reader/LundEventData.h>


// TODO repalce by some 'conventional' PDG info provider. Like TDatabasePDG
//==================================================================
  Double_t MassPI=0.139570,
         MassK=0.493677,
         MassProton=0.93827,
         MassNeutron = 0.939565,
         MassE=0.000511,
         MassMU=0.105658;
//==================================================================
 

using namespace std;
using namespace fmt;


struct MesonStructureRecord {
    double Q2_true;
    double Q2_smeared;
    double Q2_trkng;
    double Xel,Yel, Q2el;
   // double Xtrue,Ytrue, Q2true;
    double Xgen, Ygen, Q2gen;
    double Eth;
    double Ee;
    double p;
    double pt;
};

static MesonStructureRecord vmrec;


void MesonStructureLambdaProcessor::Init()
{
    ///  Called once at program start.
    print("MesonStructureLambdaProcessor::Init()\n");

    // initialize class that holds histograms and other root objects
    // 'services' is a service locator, we ask it for TFile
    root_out.init(services.Get<TFile>());

    //

    // Ask service locator for parameter manager. We want to get this plugin parameters.
    auto pm = services.Get<JParameterManager>();

    // Verbosity level. 0 = show nothing. 1 - show some. 2 - mad printer
    // SetDefaultParameter actually sets the parameter value from arguments if it is specified
    verbose = 0;
    pm->SetDefaultParameter("mstruct_lambda:verbose", verbose, "Plugin output level. 0-almost nothing, 1-some, 2-everything");

    smearing = 0;
    pm->SetDefaultParameter("mstruct_lambda:smearing", smearing, "Particle smearing 0-true MC, 1-smearing, 2-reconstruction");

    // Beams energies
    pm->SetDefaultParameter("mstruct_lambda:e_beam_energy", e_beam_energy, "Energy of colliding electron beam");
    pm->SetDefaultParameter("mstruct_lambda:ion_beam_energy", ion_beam_energy, "Energy of colliding ion beam");
    if(verbose) {
        print("Parameters:\n");
        print("  mstruct_lambda:verbose:         {0}{0}\n", verbose);
        print("  mstruct_lambda:e_beam_energy:   {}\n", e_beam_energy);
        print("  mstruct_lambda:ion_beam_energy: {}\n", ion_beam_energy);
    }
}


void MesonStructureLambdaProcessor::Process(const std::shared_ptr<const JEvent>& event) {
    ///< Called every event.
    using namespace fmt;
    using namespace std;
    using namespace minimodel;
    std::lock_guard<std::recursive_mutex> locker(root_out.lock);

    double Xtrue, Ytrue, Q2true;

    // >oO debug printing
    if (event->GetEventNumber() % 1000 == 0 && verbose > 1) {
        print("\n--------- EVENT {} --------- \n", event->GetEventNumber());
    }

    // Get the inputs needed for this factory.
    auto particles = event->Get<MesonStructureInputParticle>();

    double true_q2;
    double true_x;
    double true_s_e;
    double true_xpi;
    double true_ypi;
    double true_tpi;

    double have_true_dis_info = false;
    bool  HIT_IN_RP=false;

    // How to check you have hits
    bool i_have_hits = event->GetFactory<minimodel::McFluxHit>("", false);   // false = don't throw exception if factory is not found

    if(i_have_hits) {
        auto hits = event->Get<minimodel::McFluxHit>();
        for(auto hit: hits) {
            if(ej::StartsWith(hit->vol_name, "ffi_ZDC")) {
                // Hit is in ZDC
           //     printf("I see hits in ZDC\n");
            }
            if(ej::StartsWith(hit->vol_name, "ffi_RPOT_D2_lay")) {
                // Hit is in RPOT
                print("I see hits in RPOT x={}  y={}\n",hit->x,hit->y);
               HIT_IN_RP=true;
               root_out.h2_RP_XY->Fill(hit->x- 820.,hit->y);

            }

        }
    }


    if(event->GetFactory<ej::LundEventData>("", false)) {
        have_true_dis_info = true;
        auto eventData = event->GetSingle<ej::LundEventData>();
        true_x = eventData->var1;
        true_q2 = eventData->var2;
        true_s_e = eventData->var3;
        true_xpi = eventData->var5;
        true_ypi = eventData->var6;
        true_tpi = eventData->var7;
        root_out.h2_XQ2_true->Fill(true_x, true_q2);
        root_out.h1_Q2_true->Fill(true_q2);
        root_out.h1_X_true->Fill(true_x);

    }

    TLorentzVector pbeam, nbeam, ebeam;
    double crossing_angle=0.025; // for eRHIC
    //   double crossing_angle=0.05; // for JLEIC
    pbeam.SetXYZM(0., 0., ion_beam_energy, MassProton);
    pbeam.RotateY(crossing_angle);
    nbeam.SetXYZM(0., 0., ion_beam_energy , MassNeutron);
    nbeam.RotateY(crossing_angle);
    ebeam.SetXYZM(0., 0., e_beam_energy, MassE);

    //   --- calculate kinematic variables using electron only method -------------
    for (auto electron: particles) {

        if (abs(electron->pdg) == 11 && electron->charge == -1 ) {
            if (verbose >= 2) { print("This is a place to check kinematics X,Q2 \n"); }
            double X_em, Y_em, Q2_em;
            double X_4vec, Y_4vec, Q2_4vec;
            double E_el = electron->p.E();
            double Theta_el = 3.1415- electron->p.Theta();


            root_out.h_El_Etot->Fill(E_el);
            root_out.h_El_Theta->Fill(Theta_el*180/3.1415);


            double PISL[4] = {0., 0., e_beam_energy, sqrt(e_beam_energy * e_beam_energy + MassE * MassE)};
            double PISP[4] = { ion_beam_energy * sin(crossing_angle), 0.,-ion_beam_energy*cos(crossing_angle), sqrt(ion_beam_energy * ion_beam_energy + MassProton * MassProton)};

            double PFSL[4] = {electron->p.px(), electron->p.py(), electron->p.pz(),
                              sqrt(electron->p.E() * electron->p.E() + MassE * MassE)};

            dis::GEN_XYQ2(PFSL, PISL, PISP, X_4vec, Y_4vec, Q2_4vec);
            root_out.h2_XQ2_4vec->Fill(X_4vec, Q2_4vec);

            dis::GEN_El_XYQ2(E_el, Theta_el, e_beam_energy, ion_beam_energy, X_em, Y_em, Q2_em);
            root_out.h2_XQ2_em->Fill(X_em, Q2_em);
            // root_out.h2_XQ2_em->Fill(log10(Xtrue), log10(Q2true));
            root_out.h1_Q2_em->Fill(Q2_em);           root_out.h1_X_em->Fill(X_em);
            root_out.h1_Q2_4vec->Fill(Q2_4vec);       root_out.h1_X_4vec->Fill(X_4vec);

            if (verbose >= 2) {
                print("Meson structure :Ee {:<11} McGeneratedParticle Th {:<11}  (Xtrue {:<11} Xel {:<11}) ( Ytrue {:<11}  Yel {:<11}) Q2true {:<11} Q2el {:<11} \n",
                      electron->p.E(), electron->p.Theta(), Xtrue, X_em, Ytrue, Y_em, Q2true, Q2_em);
            }


            if (Ytrue > 0.05 && Ytrue < 0.95) { return; }

        }
        root_out.h2_XQ2_true_cuts->Fill(Xtrue, Q2true);

    }


            //*************************Loop over Generated particles *********************************
            //------------------------------------------------------------------------------

            for (auto particle1: particles) {  //----- loop -I  === start with proton!
                std::lock_guard<std::recursive_mutex> locker(root_out.lock);


                if (verbose >= 2) {
                    print("mstruct_lambda: particle {:<5} px: {:<11} py: {:<11} pz: {:<11} pt: {:<11}  ptot: {:<11} charge: {:<5} \n",
                          particle1->pdg, particle1->p.px(), particle1->p.py(), particle1->p.pz(), particle1->p.pt(), particle1->p.P(),
                          particle1->charge);
                }


                if (particle1->charge == 0) continue; // request good charge
             //   if (particle1->p.pt() < 0.01) continue;   // request min pt
            //    if (abs(particle1->pdg) != 211) continue; //  pions only
                   if (abs(particle1->pdg) != 2212) continue; // protons  only

                TLorentzVector pr, my_pr;
                pr.SetXYZM(particle1->p.px(), particle1->p.py(), particle1->p.pz(), MassProton);
                my_pr=pbeam-pr;
   //             root_out.h_ProtonPtot->Fill(particle1->p.P());
   //             root_out.h_ProtonPt->Fill(particle1->p.pt() );
   //             root_out.h_ProtonTheta->Fill(particle1->p.theta());
   //             root_out.h_t_pt_p->Fill( abs(particle1->p.pt()*particle1->p.pt()));
                root_out.h_ProtonPtot->Fill(my_pr.Mag());
                root_out.h_ProtonPt->Fill(my_pr.Pt() );
                root_out.h_ProtonTheta->Fill(my_pr.Theta()*180/3.1415);
                root_out.h_t_pt_p->Fill( abs(my_pr.Pt()*my_pr.Pt()));
                root_out.h_t_pt_p_Theta->Fill( my_pr.Theta()*180/3.1415,abs(my_pr.Pt()*my_pr.Pt()));
                  if  (HIT_IN_RP) {
                    root_out.h_t_pt_p_Theta_RP->Fill(my_pr.Theta() * 180 / 3.1415, abs(my_pr.Pt() * my_pr.Pt()));
                    root_out.h_t_pt_p_RP->Fill(abs(my_pr.Pt()*my_pr.Pt()));

                   }

                 //------------------------------------------------------------------------------
                for (auto particle2: particles) {  //----- loop -J --  pion!!
                    if (verbose >= 2) {
                        print("meson structure : particle1 {:<5},  particle2 {:<5},  \n",
                              particle1->pdg, particle2->pdg);
                    }

                    if (particle1->id == particle2->id) continue; //
                    if (particle2->charge != 0) continue; // request good charge -- pion
                    if (abs(particle2->pdg) != 111) continue; // protons  only
                    if( abs(particle2->p.eta())>3.5) continue;  // pion within acceptance of the calorimeter

                      root_out.h_Pion_Etot->Fill(particle2->p.E());
                      root_out.h_Pion_Theta->Fill(particle2->p.Theta()*180/3.1415);

                    root_out.h_PionPtot->Fill(particle2->p.P());
                    root_out.h_t_pt_p->Fill(particle1->p.pt()*particle1->p.pt());



                   }    // -- end loop -J
                //------------------------------------------------------------------------------

            } // ---- end loop -I
        }


void MesonStructureLambdaProcessor::Finish() {
    ///< Called after last event of last event source has been processed.
    print("MesonStructureLambdaProcessor::Finish(). Cleanup\n");
}



template <>
void JFactoryT<MesonStructureInputParticle>::Process(const std::shared_ptr<const JEvent>& event) {

    using namespace fmt;
    using namespace std;
    using namespace minimodel;

    // Get the inputs needed for this factory.
    auto gen_parts = event->GetFactory<McGeneratedParticle>("smear")?   // If there is a smearing factory
                     event->Get<McGeneratedParticle>("smear"):
                     event->Get<McGeneratedParticle>();


    // text_event_record has just tokenized text from

    // Fill Original particles information
    std::vector<MesonStructureInputParticle *> oc_particles;
    for (auto gen_part : gen_parts) {
        auto oc_particle = new MesonStructureInputParticle();
        oc_particle->pdg = gen_part->pdg;

        oc_particle->charge = gen_part->charge;
        oc_particle->id = gen_part->id;
//        oc_particle->p.SetPxPyPzE(gen_part->px, gen_part->py, gen_part->pz, gen_part->tot_e);
        oc_particle->p.SetPxPyPzE(gen_part->px, gen_part->py, -gen_part->pz, gen_part->tot_e);
        // ---- convert to um ------
        oc_particle->vertex.SetXYZ(gen_part->vtx_x * 1000., gen_part->vtx_y * 1000., gen_part->vtx_z * 1000.);
        oc_particles.push_back(oc_particle);
    }

    Set(std::move(oc_particles));
}
