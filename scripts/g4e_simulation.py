from g4epy import Geant4Eic

# final_state="pi_p_18on275"
final_state="pi_n_18on275"
# final_state="k_lambda_18on275"

# final_state="pi_p_10on275"
# final_state="pi_n_10on275"
# final_state="k_lambda_10on275"

# final_state="pi_n_10on100"

g4e = Geant4Eic(detector='jleic', beamline='erhic')\
         .source('MC_OUTPUTS/%s_lund.dat' % final_state)\
         .output('OUTPUTS/g4e_%s' % final_state)\
         .beam_on(10000)
         # .vis()
g4e.run()
