from pyjano.jana import Jana, PluginFromSource

mstruct_general = PluginFromSource('./mstruct_general')   # Name will be determined from folder name
# add name=<...> for custom name

# final_state="pi_p_18on275"
final_state="pi_n_18on275"
# final_state="k_lambda_18on275"

# final_state="pi_p_10on275"
# final_state="pi_n_10on275"
# final_state="k_lambda_10on275"

# final_state="pi_n_10on100"

jana = Jana(nevents=10000, output='OUTPUTS/jana_%s.root' % final_state)

# G4E reader here
jana.plugin('g4e_reader') \
    .source('OUTPUTS/g4e_%s.root' % final_state)\

# Parameters:
#     verbose   - Plugin output level. 0-almost nothing, 1-some, 2-everything
#     smearing  - Particle smearing 0-true MC, 1-smearing, 2-reconstruction");
# Beams energies. Defaults are 10x100 GeV
#     e_beam_energy    -  Energy of colliding electron beam");
#     ion_beam_energy  -  Energy of colliding ion beam");
jana.plugin(mstruct_general, verbose=1)

jana.run()
